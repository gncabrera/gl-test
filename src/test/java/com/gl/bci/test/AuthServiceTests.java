package com.gl.bci.test;

import com.gl.bci.test.exception.LoginException;
import com.gl.bci.test.exception.SignupException;
import com.gl.bci.test.model.Phone;
import com.gl.bci.test.model.Role;
import com.gl.bci.test.model.User;
import com.gl.bci.test.model.UserRole;
import com.gl.bci.test.payload.dto.PhoneDto;
import com.gl.bci.test.payload.request.SignupRequest;
import com.gl.bci.test.payload.response.JwtResponse;
import com.gl.bci.test.payload.response.LoginResponse;
import com.gl.bci.test.payload.response.SignupResponse;
import com.gl.bci.test.repository.PhoneRepository;
import com.gl.bci.test.repository.RoleRepository;
import com.gl.bci.test.repository.UserRepository;
import com.gl.bci.test.security.jwt.JwtUtils;
import com.gl.bci.test.service.AuthService;
import com.gl.bci.test.service.UserDetailsImpl;
import com.gl.bci.test.service.UserDetailsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class AuthServiceTests {
    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private PhoneRepository phoneRepository;

    @Mock
    private PasswordEncoder encoder;

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private Authentication authentication;

    @Mock
    private UserDetailsServiceImpl userDetailsService;

    @Mock
    private JwtUtils jwtUtils;

    @InjectMocks
    private AuthService authService;

    @Test
    public void testRegisterUser_Success() {

        UserDetailsImpl userDetails = new UserDetailsImpl(
                1L,
                "mail@example.com",
                "mail@example.com",
                "password",
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")));

        User user = new User();
        user.setLastLogin(LocalDateTime.MAX);
        user.setActive(true);
        user.setEmail("mail@example.com");
        user.setName("Usuario Test");
        user.setCreatedTime(LocalDateTime.MAX);
        user.setId(1L);

        // Mock data
        SignupRequest signUpRequest = new SignupRequest("Usuario Test", "mail@example.com", "password");
        signUpRequest.setRoles(Collections.singleton(UserRole.ROLE_USER));
        signUpRequest.setPhones(Collections.singletonList(new PhoneDto(1234567890L,  11, "+11")));

        when(userRepository.findByEmail(signUpRequest.getEmail())).thenReturn(Optional.of(user));
        when(userRepository.existsByEmail(signUpRequest.getEmail())).thenReturn(false);
        when(encoder.encode(signUpRequest.getPassword())).thenReturn("encodedPassword");
        when(roleRepository.findByName(any())).thenReturn(Optional.of(new Role(UserRole.ROLE_USER)));
        when(authentication.getPrincipal()).thenReturn(userDetails);
        when(userDetailsService.loadUserByUsername(any())).thenReturn(userDetails);
        when(authenticationManager.authenticate(any())).thenReturn(authentication);
        when(jwtUtils.generate(any())).thenReturn("token");


        // Perform registration
        SignupResponse response = authService.registerUser(signUpRequest);

        // Verify
        verify(userRepository, times(2)).save(any(User.class));
        verify(phoneRepository, times(1)).save(any(Phone.class));

        assertEquals(LocalDateTime.MAX, response.getCreated());
        assertTrue(response.getActive());
        assertEquals("token", response.getToken());
        assertEquals(1L, response.getId());
    }

    @Test
    public void testRegisterUser_EmailInUse() {
        // Mock data
        SignupRequest signUpRequest = new SignupRequest("John Doe", "john@example.com", "password");

        when(userRepository.existsByEmail(signUpRequest.getEmail())).thenReturn(true);

        // Perform registration and expect exception
        assertThrows(SignupException.class, () -> authService.registerUser(signUpRequest));

        // Verify that userRepository.existsByEmail is called
        verify(userRepository, times(1)).existsByEmail(signUpRequest.getEmail());

    }

    @Test
    public void testRegisterUser_RoleNotFound() {
        // Mock data
        SignupRequest signUpRequest = new SignupRequest("John Doe", "john@example.com", "password");
        signUpRequest.setRoles(Collections.singleton(UserRole.ROLE_ADMIN));

        when(userRepository.existsByEmail(signUpRequest.getEmail())).thenReturn(false);
        when(roleRepository.findByName(any())).thenReturn(Optional.empty());

        // Perform registration and expect exception
        assertThrows(SignupException.class, () -> authService.registerUser(signUpRequest));

        // Verify that roleRepository.findByName is called for each role
        verify(roleRepository, times(1)).findByName(UserRole.ROLE_ADMIN);

    }

    @Test
    void testLogin_ReturnsJwtResponse() {
        Principal principal = mock(Principal.class);
        UserDetailsImpl userDetails = mock(UserDetailsImpl.class);
        JwtResponse jwtResponse = mock(JwtResponse.class);
        User user = new User();
        user.setLastLogin(LocalDateTime.MAX);
        user.setActive(true);
        user.setEmail("mail@example.com");
        user.setName("Usuario Test");
        user.setCreatedTime(LocalDateTime.MAX);
        user.setId(1L);

        when(principal.getName()).thenReturn("user@example.com");
        when(userDetailsService.loadUserByUsername("user@example.com")).thenReturn(userDetails);
        when(jwtUtils.generate(userDetails)).thenReturn("jwt");
        when(userDetails.getAuthorities()).thenReturn(Collections.emptyList());
        when(userDetails.getEmail()).thenReturn("user@example.com");
        when(userRepository.findByEmail("user@example.com")).thenReturn(Optional.of(user));

        LoginResponse response = authService.login(principal);

        verify(userDetailsService).loadUserByUsername("user@example.com");
        verify(jwtUtils).generate(userDetails);
        verify(userDetails).getAuthorities();
        verify(userDetails, times(2)).getEmail();
        verify(userRepository, times(2)).findByEmail("user@example.com");
        verify(userRepository).save(user);

        assertNotNull(response);
        assertEquals(LocalDateTime.MAX, response.getCreated());
        assertTrue(response.getActive());
        assertEquals("jwt", response.getToken());
        assertEquals(1L, response.getId());
    }

    @Test
    void testLogin_ThrowsLoginExceptionWhenUserNotFound() {
        Principal principal = mock(Principal.class);
        UserDetailsImpl userDetails = mock(UserDetailsImpl.class);

        when(principal.getName()).thenReturn("user@example.com");
        when(userDetailsService.loadUserByUsername("user@example.com")).thenReturn(userDetails);
        when(userDetails.getEmail()).thenReturn("user@example.com");
        when(userRepository.findByEmail("user@example.com")).thenReturn(Optional.empty());

        assertThrows(LoginException.class, () -> authService.login(principal));

        verify(userDetailsService).loadUserByUsername("user@example.com");
        verify(userRepository).findByEmail("user@example.com");
    }



    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }


}
