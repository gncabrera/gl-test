package com.gl.bci.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.gl.bci.test.controller.AuthController;
import com.gl.bci.test.payload.request.SignupRequest;
import com.gl.bci.test.security.jwt.JwtUtils;
import com.gl.bci.test.service.AuthService;
import com.gl.bci.test.service.UserDetailsServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WebMvcTest(AuthController.class)
class AuthControllerValidationTests {

	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Autowired
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@MockBean
	private AuthService service;

	@MockBean
	private JwtUtils jwtUtils;

	@MockBean
	private UserDetailsServiceImpl userDetailsService;

	private final String SIGNUP_URL =  "/api/auth/signup";

	@BeforeAll
	public void beforeAll()
	{
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	private String buildJson(SignupRequest request) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(request);
		return requestJson;

	}

	private void testBadRequest(SignupRequest request) throws Exception {
		mockMvc.perform(post(SIGNUP_URL).contentType(APPLICATION_JSON_UTF8)
						.content(buildJson(request)))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testSignupIsOk() throws Exception {

		SignupRequest request = new SignupRequest();
		request.setPassword("A12asdasd");
		request.setEmail("test@email.com");

		mockMvc.perform(post(SIGNUP_URL).contentType(APPLICATION_JSON_UTF8)
						.content(buildJson(request)))
				.andExpect(status().isOk());
	}

	@Test
	public void testValidEmailDifferentTLDisOk() throws Exception {

		SignupRequest request = new SignupRequest();
		request.setPassword("A12asdasd");
		request.setEmail("test@email.com.ss");

		mockMvc.perform(post(SIGNUP_URL).contentType(APPLICATION_JSON_UTF8)
						.content(buildJson(request)))
				.andExpect(status().isOk());
	}

	@Test
	public void testMailIsCaseInsensitive() throws Exception {

		SignupRequest request = new SignupRequest();
		request.setPassword("A12asdasd");
		request.setEmail("TEST@doMain.com");

		mockMvc.perform(post(SIGNUP_URL).contentType(APPLICATION_JSON_UTF8)
						.content(buildJson(request)))
				.andExpect(status().isOk());
	}

	@Test
	public void testEmptyMailIsBadRequest() throws Exception {

		SignupRequest request = new SignupRequest();
		request.setPassword("A12asdasd");
		request.setEmail("");

		testBadRequest(request);
	}

	@Test
	public void testWhiteSpaceMailIsBadRequest() throws Exception {

		SignupRequest request = new SignupRequest();
		request.setPassword("A12asdasd");
		request.setEmail(" ");

		testBadRequest(request);
	}

	@Test
	public void testMailWithoutAtIsBadRequest() throws Exception {

		SignupRequest request = new SignupRequest();
		request.setPassword("A12asdasd");
		request.setEmail("testemail.com");

		testBadRequest(request);
	}

	@Test
	public void testMailWithoutDomainIsBadRequest() throws Exception {

		SignupRequest request = new SignupRequest();
		request.setPassword("A12asdasd");
		request.setEmail("test@");

		testBadRequest(request);
	}

	@Test
	public void testMailWithoutUsernameIsBadRequest() throws Exception {

		SignupRequest request = new SignupRequest();
		request.setPassword("A12asdasd");
		request.setEmail("@domain.com");

		testBadRequest(request);
	}

	@Test
	public void testMailWithMultipleAtIsBadRequest() throws Exception {

		SignupRequest request = new SignupRequest();
		request.setPassword("A12asdasd");
		request.setEmail("test@domain@test.com");

		testBadRequest(request);
	}

	@Test
	public void testPasswordIsEmptyIsBadRequest() throws Exception {

		SignupRequest request = new SignupRequest();
		request.setPassword("");
		request.setEmail("test@domain.com");

		testBadRequest(request);
	}

	@Test
	public void testPasswordNoUppercaseIsBadRequest() throws Exception {

		SignupRequest request = new SignupRequest();
		request.setPassword("abcdef12");
		request.setEmail("test@domain.com");

		testBadRequest(request);
	}

	@Test
	public void testPasswordTooManyUppercaseIsBadRequest() throws Exception {

		SignupRequest request = new SignupRequest();
		request.setPassword("ABCDef12");
		request.setEmail("test@domain.com");

		testBadRequest(request);
	}

	@Test
	public void testPasswordWithoutDigitsIsBadRequest() throws Exception {

		SignupRequest request = new SignupRequest();
		request.setPassword("Abcdefgh");
		request.setEmail("test@domain.com");

		testBadRequest(request);
	}

	@Test
	public void testPasswordWithTooManyDigitsIsBadRequest() throws Exception {

		SignupRequest request = new SignupRequest();
		request.setPassword("Abcde123");
		request.setEmail("test@domain.com");

		testBadRequest(request);
	}

	@Test
	public void testPasswordTooLongIsBadRequest() throws Exception {

		SignupRequest request = new SignupRequest();
		request.setPassword("Abcde123fghijklmno");
		request.setEmail("test@domain.com");

		testBadRequest(request);
	}

	@Test
	public void testPasswordTooShortIsBadRequest() throws Exception {

		SignupRequest request = new SignupRequest();
		request.setPassword("Abc123");
		request.setEmail("test@domain.com");

		testBadRequest(request);
	}

}
