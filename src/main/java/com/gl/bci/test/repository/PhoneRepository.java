package com.gl.bci.test.repository;

import com.gl.bci.test.model.Phone;
import com.gl.bci.test.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PhoneRepository extends JpaRepository<Phone, Long> {
}
