package com.gl.bci.test.service;

import com.gl.bci.test.exception.LoginException;
import com.gl.bci.test.exception.SignupException;
import com.gl.bci.test.model.Phone;
import com.gl.bci.test.model.Role;
import com.gl.bci.test.model.User;
import com.gl.bci.test.model.UserRole;
import com.gl.bci.test.payload.dto.PhoneDto;
import com.gl.bci.test.payload.request.LoginRequest;
import com.gl.bci.test.payload.request.SignupRequest;
import com.gl.bci.test.payload.response.JwtResponse;
import com.gl.bci.test.payload.response.LoginResponse;
import com.gl.bci.test.payload.response.SignupResponse;
import com.gl.bci.test.repository.PhoneRepository;
import com.gl.bci.test.repository.RoleRepository;
import com.gl.bci.test.repository.UserRepository;
import com.gl.bci.test.security.jwt.JwtUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AuthService {

    private AuthenticationManager authenticationManager;
    private UserRepository userRepository;
    private RoleRepository roleRepository;

    private PhoneRepository phoneRepository;
    private PasswordEncoder encoder;
    private JwtUtils jwtUtils;
    private UserDetailsServiceImpl userDetailsService;

    public AuthService(AuthenticationManager authenticationManager, UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder encoder, JwtUtils jwtUtils, PhoneRepository phoneRepository, UserDetailsServiceImpl userDetailsService) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
        this.jwtUtils = jwtUtils;
        this.phoneRepository = phoneRepository;
        this.userDetailsService = userDetailsService;
    }

    @Transactional
    public SignupResponse registerUser(SignupRequest signUpRequest) {
        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            throw new SignupException("Error: Email is already in use!");
        }

        // Create new user's account
        User user = new User(signUpRequest.getName(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));

        user.setActive(true);
        Set<UserRole> strRoles = signUpRequest.getRoles();
        Set<Role> roles = new HashSet<>();

        if(CollectionUtils.isEmpty(strRoles)) {
            strRoles = new HashSet<>(Arrays.asList(UserRole.ROLE_USER));
        }

        strRoles.forEach(role -> {
            Role adminRole = roleRepository.findByName(role)
                    .orElseThrow(() -> new SignupException("Error: Role " +  role + " is not found."));
            roles.add(adminRole);

        });

        user.setRoles(roles);

        userRepository.save(user);

        signUpRequest.getPhones().forEach(dto -> {
            Phone phone = PhoneDto.buildPhoneFromDto(dto);
            phone.setUser(user);
            phoneRepository.save(phone);
        });

        JwtResponse jwtResponse = performLogin(signUpRequest.getEmail());
        User loggedInUser = userRepository.findByEmail(signUpRequest.getEmail()).orElseThrow(() -> new LoginException("User with email not found!"));

        return new SignupResponse(loggedInUser, jwtResponse.getAccessToken());
    }

    public LoginResponse login(Principal principal) {
        JwtResponse jwtResponse = performLogin(principal.getName());
        User loggedInUser = userRepository.findByEmail(principal.getName()).orElseThrow(() -> new LoginException("User with email not found!"));
        return LoginResponse.build(jwtResponse, loggedInUser);
    }

    public JwtResponse performLogin(String username) {
        UserDetailsImpl userDetails = (UserDetailsImpl)userDetailsService.loadUserByUsername(username);
        String jwt = jwtUtils.generate(userDetails);
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        User user = userRepository.findByEmail(userDetails.getEmail()).orElseThrow(() -> new LoginException("User with email not found!"));
        user.setLastLogin(LocalDateTime.now());
        userRepository.save(user);

        return new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles);
    }
}
