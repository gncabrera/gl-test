package com.gl.bci.test.service;

import com.gl.bci.test.payload.response.RequestError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;

@RestControllerAdvice
public class GlobalControllerExceptionHandler {

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<RequestError> internalServerError(Exception ex) {
        return ResponseEntity
                .internalServerError()
                .body(new RequestError().addErrorDetail(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage()));
    }
}
