package com.gl.bci.test.controller;

import com.gl.bci.test.exception.LoginException;
import com.gl.bci.test.exception.SignupException;
import com.gl.bci.test.payload.request.LoginRequest;
import com.gl.bci.test.payload.request.SignupRequest;
import com.gl.bci.test.payload.response.LoginResponse;
import com.gl.bci.test.payload.response.RequestError;
import com.gl.bci.test.payload.response.SignupResponse;
import com.gl.bci.test.service.AuthService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @GetMapping("/login")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> login(Principal principal) {
        try {
            LoginResponse response = authService.login(principal);
            return ResponseEntity.ok(response);
        } catch(LoginException e) {
            return ResponseEntity
                    .badRequest()
                    .body(new RequestError().addErrorDetail(HttpServletResponse.SC_BAD_REQUEST, e.getMessage()));
        }
    }



    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        try {
            SignupResponse signupResponse = authService.registerUser(signUpRequest);
            return ResponseEntity.ok(signupResponse);
        } catch (SignupException e) {
            return ResponseEntity
                    .badRequest()
                    .body(new RequestError().addErrorDetail(HttpServletResponse.SC_BAD_REQUEST, e.getMessage()));
        }


    }
}
