package com.gl.bci.test.payload.request;

import com.gl.bci.test.model.UserRole;
import com.gl.bci.test.payload.dto.PhoneDto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class SignupRequest {
    private String name;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    private Set<UserRole> roles;

    @NotBlank
    @Pattern(regexp="^[a-z0-9]*[A-Z][a-z0-9]*$", message = "must contain one Uppercase letter") // One uppercase letter
    @Pattern(regexp="^[a-zA-Z]*[0-9][a-zA-Z]*[0-9][a-zA-Z]*$", message = "must contain two digits") // Two digits
    @Size(min = 8, max = 12)
    private String password;

    private List<PhoneDto> phones;


    public SignupRequest() {
    }

    public SignupRequest(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public List<PhoneDto> getPhones() {
        return phones;
    }

    public void setPhones(List<PhoneDto> phones) {
        this.phones = phones;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<UserRole> getRoles() {
        return this.roles;
    }

    public void setRoles(Set<UserRole> roles) {
        this.roles = roles;
    }
}
