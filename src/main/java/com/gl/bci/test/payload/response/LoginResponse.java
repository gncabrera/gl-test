package com.gl.bci.test.payload.response;

import com.gl.bci.test.model.Phone;
import com.gl.bci.test.model.User;
import com.gl.bci.test.payload.dto.PhoneDto;
import com.gl.bci.test.payload.request.LoginRequest;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class LoginResponse {

    private Long id;
    private LocalDateTime created;
    private LocalDateTime lastLogin;
    private String token;
    private Boolean isActive;
    private String name;
    private String email;
    private List<PhoneDto> phones;
    private List<String> roles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(LocalDateTime lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<PhoneDto> getPhones() {
        return phones;
    }

    public void setPhones(List<PhoneDto> phones) {
        this.phones = phones;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public static LoginResponse build(JwtResponse jwtResponse, User user) {
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setId(user.getId());
        loginResponse.setCreated(user.getCreatedTime());
        loginResponse.setLastLogin(user.getLastLogin());
        loginResponse.setToken(jwtResponse.getAccessToken());
        loginResponse.setActive(user.getActive());
        loginResponse.setName(user.getName());
        loginResponse.setEmail(user.getEmail());
        if(user.getPhones() != null)
            loginResponse.setPhones(user.getPhones().stream().map(ph -> new PhoneDto(ph)).collect(Collectors.toList()));
        loginResponse.setRoles(jwtResponse.getRoles());
        return loginResponse;
    }
}
