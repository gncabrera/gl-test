package com.gl.bci.test.payload.response;

import com.gl.bci.test.model.User;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class SignupResponse {
    private Long id;
    private LocalDateTime created;
    private LocalDateTime lastLogin;
    private String token;
    private Boolean isActive;

    public SignupResponse() {
    }

    public SignupResponse(User user, String token) {
        this.id = user.getId();
        this.created = user.getCreatedTime();
        this.lastLogin = user.getLastLogin();
        this.token = token;
        this.isActive = user.getActive();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(LocalDateTime lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }


}
