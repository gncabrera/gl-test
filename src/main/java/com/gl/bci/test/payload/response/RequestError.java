package com.gl.bci.test.payload.response;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;

public class RequestError {
    private ArrayList<RequestErrorDetail> error = new ArrayList<>();

    public ArrayList<RequestErrorDetail> getError() {
        return error;
    }

    public void setError(ArrayList<RequestErrorDetail> error) {
        this.error = error;
    }

    public RequestError addErrorDetail(Integer codigo, String detail) {
        error.add(new RequestErrorDetail(new Date(), codigo, detail));
        return this;
    }
}
