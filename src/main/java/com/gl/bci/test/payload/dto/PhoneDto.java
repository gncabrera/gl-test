package com.gl.bci.test.payload.dto;

import com.gl.bci.test.model.Phone;

public class PhoneDto {
    private Long number;
    private Integer citycode;
    private String contrycode;

    public PhoneDto() {
    }

    public PhoneDto(Phone phone) {
        this.citycode = phone.getCitycode();
        this.contrycode = phone.getCountrycode();
        this.number = phone.getNumber();
    }

    public PhoneDto(Long number, Integer citycode, String contrycode) {
        this.number = number;
        this.citycode = citycode;
        this.contrycode = contrycode;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Integer getCitycode() {
        return citycode;
    }

    public void setCitycode(Integer citycode) {
        this.citycode = citycode;
    }

    public String getContrycode() {
        return contrycode;
    }

    public void setContrycode(String contrycode) {
        this.contrycode = contrycode;
    }

    public static Phone buildPhoneFromDto(PhoneDto dto) {
        return new Phone(dto.getNumber(), dto.getCitycode(), dto.getContrycode());
    }
}
