package com.gl.bci.test.model;

public enum UserRole {
    ROLE_USER,
    ROLE_ADMIN
}
