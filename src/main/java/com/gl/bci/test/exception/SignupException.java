package com.gl.bci.test.exception;

public class SignupException extends RuntimeException{

    public SignupException() {
        super();
    }

    public SignupException(String message) {
        super(message);
    }
}
