## Prerequisites

- Java Development Kit (JDK) 8
- Gradle 6.x.x or higher

## Build the project
Run the following command to build the project:
```
gradlew build
```
This command will compile the source code, run tests, and generate the executable JAR file.

## Run the project
Once the project is built, you can run it with the following command:
```
gradlew bootRun
```
The project will be executed on the default port 8080. You can access it in your web browser at http://localhost:8080.

## Endpoints
### POST http://localhost:8080/api/auth/signup
Used to create a user.
Valid roles are `ROLE_USER` and `ROLE_ADMIN`
```
curl --location 'http://localhost:8080/api/auth/signup' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "user",
    "email": "email@example.com",
    "password": "p45Perfefge",
    "phones": [
        {
            "number": 123456,
            "citycode": 11,
            "contrycode": "+10"
        }
    ],
    "roles": ["ROLE_USER"]
}'
```

### GET http://localhost:8080/api/auth/login
Used to obtain logged-in user data. Use the token from the signup response.
```
curl --location 'http://localhost:8080/api/auth/login' \
--header 'Authorization: Bearer jwtToken'
```

### GET http://localhost:8080/api/test/user
Data only visible to logged-in users with `ROLE_USER` role. Use the token from the signup response.

```
curl --location 'http://localhost:8080/api/test/user' \
--header 'Authorization: Bearer jwtToken'
```

### GET http://localhost:8080/api/test/admin
Data only visible to logged-in users with `ROLE_ADMIN` role. Use the token from the signup response.

```
curl --location 'http://localhost:8080/api/test/admin' \
--header 'Authorization: Bearer jwtToken'
```

## Documentation

### Component diagram
![image info](./images/component-diagram.jpg)

### Sequence diagram: AuthController.login

![image info](./images/AuthController_login.jpg)

### Sequence diagram: AuthController.registerUser

![image info](./images/AuthController_registerUser.jpg)